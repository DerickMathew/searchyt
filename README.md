# Search Yt

A simple UI interface to search for youtube videos. 

## Setup 

- Copy the file named `.env.local.copy` in the root folder, next to it and name the copied file `.env.local`

- Fill your in your youtube apiKey within the new file `.env.local` (If you dont already have a youtube apiKey visit: https://developers.google.com/youtube/v3/getting-started)

- Install dependencies
```
npm i
```

- Run tests
```
npm run test
```

- Run the dev server
```
npm run dev
```

## Version
- Node - `12.18.2`

- NPM - `6.14.5`