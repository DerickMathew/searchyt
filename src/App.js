import React, { Component } from 'react';
import produce from 'immer';

import Header from './components/header';
import VideoList from './components/videoList';
import getVideosAndToken from './services/youtubeApiService';

import './App.scss';


class App extends Component {
    constructor() {
        super();
        this.state = { 
            searchString: '',
            videos: [],
            nextPageToken: ''
        };
    }

    async beginNewSearch(searchString) {
        this.setState(produce(this.state, draft => {
            draft.searchString = searchString;
        }));
        
        const response = await getVideosAndToken(searchString);
        this.setState(produce(this.state, draft => {
            draft.videos = [];
            response.videos.map((video) => draft.videos.push(video));
            if (response.nextPageToken) {
                draft.nextPageToken = response.nextPageToken;
            }
        }));
    }

    onFetchingMore(response) {
        this.setState(produce(this.state, draft => {
            response.videos.map((video) => draft.videos.push(video));
            if (response.nextPageToken) {
                draft.nextPageToken = response.nextPageToken;
            }
        }));
    }
  
    render() {
        return (
            <div className="App">
                <Header searchString={this.state.searchString} onClick={this.beginNewSearch.bind(this)} />
                <VideoList videos={this.state.videos} nextPageToken={this.state.nextPageToken} onFetchingMore={this.onFetchingMore.bind(this)} />
            </div>
            );
    }
}

export default App;
