import React, { Component } from 'react';
import produce from 'immer';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './style/header.scss'

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchString: props.searchString || ''
        };
        this.onClick = this.props.onClick;
    }

    updateSearch = (event) => {
        this.setState(produce(this.state, draft => {
            draft.searchString = event.target.value;
        }));
    };

    onKeyPress(event) {
        if (event.which === 13 /* Enter */) {
            event.preventDefault();
            this.onIconPress()
        }
    }

    onIconPress = () => {
        if (this.onClick)  {
            this.onClick(this.state.searchString);
        }
    }

    render() {
        const { searchString } = this.state;

        return (
            <div className='header'>
                <form className='searchBarForm' onKeyPress={this.onKeyPress.bind(this)}>
                    <div className='inputWrapper'>
                        <input type='text' className='searchInput' value={searchString} onChange={this.updateSearch.bind(this)} />
                    </div>
                    <div className='iconWrapper'>
                        <FontAwesomeIcon icon={faSearch}  onClick={this.onIconPress.bind(this)} type='submit' onSubmit={this.onIconPress.bind(this)} />
                    </div>
                </form>
            </div>
      );
    }
}

export default Header;
