import React, { Component } from 'react';
import { faClock } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import debounce from 'lodash/debounce';

import getVideosAndToken from '../services/youtubeApiService';
import VideoTile from './videoTile';

import './style/videoList.scss'

class VideoList extends Component {
    constructor(props) {
        super(props);
        this.isfetching = false;

        window.addEventListener('scroll', debounce(this.handleScroll.bind(this),200));
    }

    handleScroll() {
        if (this.isfetching) {
            // Nothing to do, here to ensure we dont rush the process
            return;
        }

        const bottomReached = window.innerHeight + document.documentElement.scrollTop === document.documentElement.offsetHeight
        if (bottomReached) {
            this.isfetching = true;
            this.fetchMore();
        }
    }

    async fetchMore() {
        if (this.props.onFetchingMore) {
            const response = await getVideosAndToken('', this.props.nextPageToken);
            this.props.onFetchingMore(response)
            this.isfetching = false;
        }

    }

    renderEmptyCollection() {
        return (
            <div className='emptyCollectionWrapper'>
                <span className='iconWrapper'>
                    <FontAwesomeIcon icon={faClock}/>
                </span>
                <span> Go ahead and type in what you'd like to find videos on</span>
            </div>
        );
    }

    renderVideoTiles() {
        const videoTiles = this.props.videos.map((video) => <VideoTile video={video} key={video.videoId}/>)

        return (
            <div className='videoList'>
                {videoTiles}
            </div>
        );
    }

    render() {
        let viewContents = this.renderEmptyCollection();
        const videos = this.props.videos ? this.props.videos : [];
        if (videos.length > 0) {
            viewContents = this.renderVideoTiles();
        }

        return (
            <div className='videoListWrapper'>
                {viewContents}
            </div>
        )
    }
}

export default VideoList;
