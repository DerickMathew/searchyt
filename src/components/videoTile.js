import React, { Component } from 'react';
import { faTv } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import decode from 'unescape';

import './style/videoTile.scss'

class VideoTile extends Component {
    render() {
        const video = this.props.video;
        const linkToVideo = video.getVideoLink();

        return (
            <a className='videoTileWrapper' href={linkToVideo} target='_blank' rel='noopener noreferrer'>
                <div className='videoTile'>
                    <div className='videoWrapper'>
                        <span className='thumbnail'>
                            <img src={video.getImageUrl()} alt="Thumbnail" />
                        </span>

                        <span className='videoTextWrapper'>
                            <div className='title'>{decode(video.title)}</div>
                            <div className='description'>{decode(video.description)}</div>
                            <div className='channelTitle'>
                                <FontAwesomeIcon icon={faTv}/> {decode(video.channelTitle)}
                            </div>
                        </span>
                    </div>
                </div>
            </a>
        );
    }
}

export default VideoTile;
