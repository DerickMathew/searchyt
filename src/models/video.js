class Video {
    constructor(data) {
        this.videoId = (data.id && data.id.videoId) ? data.id.videoId : '';
        this.title = (data.snippet && data.snippet.title) ? data.snippet.title : '';
        this.description = (data.snippet && data.snippet.description) ? data.snippet.description : '';
        this.thumbnail = {};
        if (data.snippet.thumbnails && data.snippet.thumbnails.default) {
            this.thumbnail = data.snippet.thumbnails.default
        }
        this.channelTitle = (data.snippet && data.snippet.channelTitle) ? data.snippet.channelTitle : '';
    }

    getVideoLink() {
        const YOUTUBE_WATCH_URL = process.env.REACT_APP_YOUTUBE_WATCH_URL;
        const videoId = this.videoId;

        return videoId.length > 0 ? `${YOUTUBE_WATCH_URL}?v=${videoId}`: '';
    }

    getImageUrl() {
        return this.thumbnail.url ? this.thumbnail.url : '';
    }
}

export default Video;
