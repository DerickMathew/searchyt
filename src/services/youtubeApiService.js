import axios from 'axios';
import createDOMPurify from 'dompurify';
import { JSDOM } from 'jsdom';

import Video from '../models/video';

const YOUTUBE_APIKEY = process.env.REACT_APP_YOUTUBE_APIKEY;
const YOUTUBE_SEARCH_URL = process.env.REACT_APP_YOUTUBE_SEARCH_URL;
const PAGE_SIZE = 25;

export const getParams = (query, pageToken) => {
    const params = {
        part: 'snippet',
        maxResults: PAGE_SIZE,
        key: YOUTUBE_APIKEY
    }

    if (pageToken) {
        params.pageToken = pageToken;
    } else {
        params.q = query;
    }
    return params;
}

const searchYoutube = (query, pageToken) => {
        const params = getParams(query, pageToken);
        return axios.get(YOUTUBE_SEARCH_URL, {
            params: params,
            responseType: 'json'
        });
}

const getNextPageToken = (responseJson) => {
    return responseJson.nextPageToken ? responseJson.nextPageToken : ''; 
}

const getVideos = (responseJson) => {
    const videos = [];
    if(responseJson.items) {
        responseJson.items.map((item) => videos.push(new Video(item)));
    }

    return videos;
}

const getCleanedQueryString = (query) => {
    const windowEmulator = new JSDOM('').window;
    const DOMPurify = createDOMPurify(windowEmulator);

    if (DOMPurify.isSupported) {
        return DOMPurify.sanitize(query); 
    }

    return query;
}

/**
 * 
 * @param string query 
 * @param string pageToken 
 * 
 * @returns {string, [Video]} NextPageToken, Videos
 */
export const getVideosAndToken = async (query, pageToken) => {
    query = query ? query : '';
    pageToken = pageToken ? pageToken : '';
    // Santizing vs escaping html
    // const queryString = getCleanedQueryString(query);
    const queryString = escape(query.trim());
    if (queryString.length || pageToken.length) {
        const responseJson = await searchYoutube(queryString, pageToken);
        const nextPageToken = getNextPageToken(responseJson.data);
        const videos = getVideos(responseJson.data);

        return { nextPageToken, videos }
    }
    return { nextPageToken: null, videos: [] }
}

export default getVideosAndToken;