import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { mount, configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import axios from 'axios';
import App from '../App';
import getSavedResponse from './helper';

configure({ adapter: new Adapter() });

test('renders the SearchBar', () => {
    const { container } = render(<App />);

    expect(container.querySelector('.searchInput')).toBeInTheDocument;
    expect(container.querySelector('.fa-search')).toBeInTheDocument;
});


test('renders the message to perform a search on loading up the app', () => {
    const { getByText } = render(<App />);
    const textMessage = getByText(/Go ahead and type in what you'd like to find videos on/i);

    expect(textMessage).toBeInTheDocument();
});

test('Returns the videos and the next page\'s token when searching for videos', () => {
    jest.mock('axios');
    axios.get = jest.fn();
    const resolvedValueData = getSavedResponse();
    axios.get.mockResolvedValue({ data: resolvedValueData });
    const app = mount(<App />);
    
    app.find('.searchInput').simulate('change', { target: { value: 'Good Day' } });
    app.find('.fa-search').simulate('click');
    app.update();

    expect(axios.get).toHaveBeenCalled();
});


