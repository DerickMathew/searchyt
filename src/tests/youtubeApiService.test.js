
import axios from 'axios';

import { getParams, getVideosAndToken } from '../services/youtubeApiService';
import getSavedResponse from './helper';

describe('getVideosAndToken',  () => {
    it('should return 25 videos and the next page token if a query is given', async () => {
        axios.get = jest.fn();
        const resolvedValueData = getSavedResponse();
        axios.get.mockResolvedValue({ data: resolvedValueData });
        const expectedToken = 'CBkQAA';

        const actual = await getVideosAndToken('test');

        expect(actual.nextPageToken).toBe(expectedToken);
        expect(actual.videos.length).toBe(25);
    });

    it('should return 25 videos and the next page token if a page token is given', async () => {
        axios.get = jest.fn();
        const resolvedValueData = getSavedResponse();
        axios.get.mockResolvedValue({ data: resolvedValueData });
        const expectedToken = 'CBkQAA';

        const actual = await getVideosAndToken('', 'test');

        expect(actual.nextPageToken).toBe(expectedToken);
        expect(actual.videos.length).toBe(25);
    });

    it('should return neither videos nor the next page token if no query or page token given', async () => {
        axios.get = jest.fn();
        const resolvedValueData = getSavedResponse();
        axios.get.mockResolvedValue({ data: resolvedValueData });

        const actual = await getVideosAndToken();

        expect(actual.nextPageToken).toBe(null);
        expect(actual.videos.length).toBe(0);
    });

    it('should return neither videos nor the next page token if only white spaces in query', async () => {
        axios.get = jest.fn();
        const resolvedValueData = getSavedResponse();
        axios.get.mockResolvedValue({ data: resolvedValueData });

        const actual = await getVideosAndToken('                                ');

        expect(actual.nextPageToken).toBe(null);
        expect(actual.videos.length).toBe(0);
    });
});